import java.util.Map;
import java.util.HashMap;

class Solution {
    public int lengthOfLongestSubstring(String s) {
        int len = s.length();
        if (len == 0) {
            return 0;
        }

        int max = 1;
        int count = 1;
        boolean isFinish = false;
        for (int i = 0; i < len && !isFinish; i++, count = 1) {
            Map<Character, Integer> m = new HashMap<Character, Integer>();
            char orig = s.charAt(i);
            m.put(orig, i);
            // System.out.println(".........i=" + i + ", orig=" + orig);
            for (int j = i + 1; j < len; j++) {
                if (!m.containsKey(s.charAt(j))) {
                    m.put(s.charAt(j), 1);
                    count++;
                    if (j == len -1) {
                        isFinish = true;
                        // System.out.println("finish!!");
                    }
                } else {
                    break;
                }
            }
            if (count > max) {
                max = count;
            }
            m = null;
            // System.out.println("=============count=" + count + ", max=" + max);
        }
        return max;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution abcababcab");
            return;
        }

        Solution solution = new Solution();
        int ret = solution.lengthOfLongestSubstring(args[0]);
        System.out.println("ret=" + ret);
    }
}