class Solution {
    public int mySqrt(int x) {
        if (x < 0) {
            return -1;
        } else if (x == 0) {
            return 0;
        } else if (x == 1) {
            return 1;
        }

        long r = (long)square(x, 0, x);
        // System.out.println("r=" + r);
        // System.out.println("r-1^=" + (r - 1) * (r - 1));
        // System.out.println("r^=" + r * r);
        // System.out.println("r+1^=" + (r + 1) * (r + 1));
        if ((r - 1) * (r - 1) < x && r * r > x) {
            return (int)(r -1);
        } else if (r * r <= x && (r + 1) * (r + 1) > x) {
            return (int)r;
        } else {
            return (int)(r + 1);
        }

    }

    public float square(float X, float low, float high) {
        float tmp = (low + high)/2;

        float Tmp = tmp * tmp;
        if (Math.abs(high - low) < 1) {
            // System.out.println("if,   tmp=" + tmp);
            return tmp;
        } else {
            float l, h;
            if (Tmp > X) {
                l = low;
                h = tmp;
            } else {
                l = tmp;
                h = high;
            }
            // System.out.println("else, tmp=" + tmp + ", l=" + l + ", h=" + h);
            return square(X, l, h);
        }
    }

/*    public float square(float X, float low, float high) {
        float tmp = (low + high)/2;

        float Tmp = tmp * tmp;
        if (Math.abs(tmp * tmp - X) < 1) {
            System.out.println("if,   tmp=" + tmp);
            return tmp;
        } else {
            float l, h;
            if (Tmp > X) {
                l = low;
                h = tmp;
            } else {
                l = tmp;
                h = high;
            }
            System.out.println("else, tmp=" + tmp + ", l=" + l + ", h=" + h);
            return square(X, l, h);
        }
    }*/

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 1256");
            return;
        }

        Solution solution = new Solution();
        int ret = solution.mySqrt(Integer.valueOf(args[0]));
        System.out.println(ret);
    }
}
