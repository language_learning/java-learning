class Solution {
    public int climbStairs(int n) {
        if (n == 0 || n == 1) {
            return n;
        }

        int bp[] = new int[n];
        bp[0] = 1;
        bp[1] = 2;
        for (int i = 2; i < n; i++) {
            bp[i] = bp[i-1] + bp[i-2];
        }

        return bp[n-1];
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 5");
            return;
        }

        Solution solution = new Solution();
        int ret = solution.climbStairs(Integer.parseInt(args[0]));
        System.out.println("ret=" + ret);
    }
}