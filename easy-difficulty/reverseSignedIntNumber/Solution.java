class Solution {
    public int reverse(int x) {
        boolean isNegative = false;
        long tmp = x;
        if (x < 0) {
            isNegative = true;
            tmp = Math.abs(x);
        }

        long reverse = 0;
        int remainder = 0;
        while (tmp > 0) {
            remainder = (int)(tmp % 10);
            reverse = reverse * 10 + remainder;
            tmp = tmp / 10;
        }

        if (isNegative) {
            reverse = 0 - reverse;
        }
        // System.out.println("reverse=" + reverse);

        if (reverse > Integer.MAX_VALUE || reverse < Integer.MIN_VALUE) {
            return 0;
        }

        return (int)reverse;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  Solution 12345");
            return;
        }

        Solution solution = new Solution();
        int ret = solution.reverse(Integer.valueOf(args[0]));
        System.out.println("reverse=" + ret);
    }
}