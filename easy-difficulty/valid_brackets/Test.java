import java.util.HashMap;
import java.util.LinkedList;
import java.util.Deque;

class Test {
   public static boolean isValid(String s) {
        if (s == null || s.length() == 0)
            return false;

        System.out.println("isValid(), s=" + s);
        // Get the length of input string
        int n = s.length();
        // Return false in special case
        if (n % 2==1) {
            return false;
        }

        // Use HashMap to store specify brackets.
        HashMap<Character, Character> pairs = new HashMap<Character, Character>(){
            {
                put(')', '(');
                put(']', '[');
                put('}', '{');
            }
        };

        LinkedList<Character> stack = new LinkedList<>();

        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            System.out.println("i=" + i + ", c=" + c);
            if (pairs.containsKey(c)) {
                System.out.println("Checking " + c);
                if (stack.isEmpty() || stack.peek() != pairs.get(c)) {
                    return false;
                }
                stack.pop();
            }else {
                stack.push(c);
            }
        }

        return stack.isEmpty();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Please input one string!");
            return;
        }

        boolean valid = isValid(args[0]);
        System.out.println("valid: " + valid);

    }
}