class A
{
    static {
        System.out.println("a");
    }

    public A() {
        System.out.println("A1");
    }

    public A(int num) {
        System.out.println("A2");
        System.out.println(num);
    }
}
