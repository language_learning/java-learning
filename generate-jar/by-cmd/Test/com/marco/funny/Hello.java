package com.marco.funny;

public class Hello {
    public Hello() {
        System.out.println("Hello");
    }

    public Hello(String str) {
        System.out.println(str);
    }

    public Hello(String str, int repeat) {
        for (int i = 0; i < repeat; i++) {
            System.out.println(str);
        }
    }

    public static final void main(String[] args) {
        Hello h1 = new Hello();
        if (args.length != 2) {
            System.out.println("Number of args is not 2, return!");
            return;
        }
        Hello h2 = new Hello(args[0]);
        Hello h3 = new Hello(args[1], 5);
    }
}
